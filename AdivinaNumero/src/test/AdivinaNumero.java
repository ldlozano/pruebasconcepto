package test;

public class AdivinaNumero {
	public static final String M1 = "Gran Adivino" ;
	public static final String M2 = "Experto" ;
	public static final String M3 = "Gran rival" ;
	private int adivinado;
	private int Intentos = 0;
	
	public AdivinaNumero(int n) {
		adivinado = n;
	}
	
	public boolean ChequeoPartida(){
		if (Intentos == 10 ) {
			return false;
		}
		return true;
		
	}
	
	
	public int ChequeoIntento(int y){
		Intentos=Intentos+1;
		
		if (y==adivinado) {
			return 0;
		}
		if (y > adivinado) {
			return -1;
		}
		if (y < adivinado) {
			return 1;
		}
		return 2;
	}
	
	
	

}
