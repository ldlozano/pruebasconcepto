package test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class AdivinaNumeroTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testChequeoIgual() {
		int expected = 0;
		AdivinaNumero a = new AdivinaNumero(78);
		int y = 78; //Numero del usuario
		int actual = a.ChequeoIntento(y);
		assertEquals(expected, actual);
	}
	
	@Test
	public void testChequeoMenor() {
		int expected = -1;
		AdivinaNumero a = new AdivinaNumero(78);
		int y = 80; //Numero del usuario
		int actual = a.ChequeoIntento(y);
		assertEquals(expected, actual);
	}
	
	@Test
	public void testChequeoMayor() {
		int expected = 1;
		AdivinaNumero a = new AdivinaNumero(78);
		int y = 76; //Numero del usuario
		int actual = a.ChequeoIntento(y);
		assertEquals(expected, actual);
	}
	
	@Test
	public void testChequeoPartida() {
		AdivinaNumero a = new AdivinaNumero(78);
		boolean response = a.ChequeoPartida();
		assertTrue("Termino partida", response);

	}
	
	@Test
	public void testAdivinaNumero() {
		AdivinaNumero adv = new AdivinaNumero(78);
		
		
		
		
		for(int a=0;a<10;a++){
			int response = adv.ChequeoIntento(50);
			
		}
		int[10] a≃
		AdivinaNumero a = new AdivinaNumero(78);
		boolean response = a.ChequeoPartida();
		assertTrue("Termino partida", response);

	}
	
	
}
