package co.com.eafit.conferre.conference.web.beans;

import java.util.List;

import javax.faces.bean.ManagedBean;

import org.primefaces.model.DefaultScheduleModel;
import org.primefaces.model.ScheduleModel;

import co.com.eafit.conferre.conferencias.business.ConferenciasFacade;

@ManagedBean
public class ConferenceView {
	ConferenciasFacade cfacade;
	private String espacio;
	private ScheduleModel eventosEspacio = new DefaultScheduleModel();

	public ScheduleModel getEventosEspacio() {
		return eventosEspacio;
	}

	public void setEventosEspacio(ScheduleModel eventosEspacio) {
		this.eventosEspacio = eventosEspacio;
	}

	public String getEspacio() {
		return espacio;
	}

	public void setEspacio(String espacio) {
		System.out.println("Fijando espacio *******************************");
		this.espacio = espacio;
	}

	public String mostrarCalendario() {
		System.out
				.println("********************************************************");
		System.out.println("Espacio " + espacio);
		// cfacade.obtenerEventosActuales();
		return "calendar.jsf";
	}

}
