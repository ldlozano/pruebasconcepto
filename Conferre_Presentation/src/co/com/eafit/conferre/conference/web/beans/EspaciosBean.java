package co.com.eafit.conferre.conference.web.beans;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

@ManagedBean
@SessionScoped
public class EspaciosBean {
	public EspaciosBean(String id, String code, int capacidad, String latitud,
			String longitud) {
		this.id = id;
		this.code = code;
		this.capacidad = capacidad;
		this.ubicacion = new Location();
		ubicacion.setLatitud(latitud);
		ubicacion.setLongitud(longitud);
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public int getCapacidad() {
		return capacidad;
	}

	public void setCapacidad(int capacidad) {
		this.capacidad = capacidad;
	}

	public Location getUbicacion() {
		return ubicacion;
	}

	public void setUbicacion(Location ubicacion) {
		this.ubicacion = ubicacion;
	}

	private String id;
	private String code;
	private int capacidad;
	private Location ubicacion;
	
	private class Location{
		private String longitud;
		private String latitud;
		public String getLongitud() {
			return longitud;
		}
		public void setLongitud(String longitud) {
			this.longitud = longitud;
		}
		public String getLatitud() {
			return latitud;
		}
		public void setLatitud(String latitud) {
			this.latitud = latitud;
		}
	}

}
