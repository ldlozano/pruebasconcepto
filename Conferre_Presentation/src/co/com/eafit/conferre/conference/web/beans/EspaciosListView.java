package co.com.eafit.conferre.conference.web.beans;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

@ManagedBean
@RequestScoped
public class EspaciosListView {
	
	private List<EspaciosBean> espacios;
	


	@PostConstruct
	public void init() {
		System.out.println("Llamando al init()");
		espacios = new ArrayList<EspaciosBean>();
		espacios.add(new EspaciosBean("1", "ES-0001", 40, "101010", "2202020"));
		espacios.add(new EspaciosBean("2", "ES-0002", 100, "101010", "2202020"));
		espacios.add(new EspaciosBean("3", "ES-0003", 60, "101010", "2202020"));
		espacios.add(new EspaciosBean("4", "ES-0004", 80, "101010", "2202020"));
		espacios.add(new EspaciosBean("5", "ES-0005", 20, "101010", "2202020"));
		// TODO Auto-generated constructor stub
	}

	public void setEspacios(List<EspaciosBean> espacios) {
		this.espacios = espacios;
	}

	public List<EspaciosBean> getEspacios() {
		return espacios;
	}
	
	
}
