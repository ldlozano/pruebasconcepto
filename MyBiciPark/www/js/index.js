/**
 * @author: Chris Hjorth, www.chrishjorth.com
 */
var jqmReady = $.Deferred();
var pgReady = $.Deferred();
var app = {
	// Callback for when the app is ready
	callback : null,
	// Application Constructor
	initialize : function(callback) {
		this.callback = callback;
		var browser = document.URL.match(/^https?:/);
		if (browser) {
			console.log("Is web.");
			// In case of web we ignore PG but resolve the Deferred Object to
			// trigger initialization
			pgReady.resolve();
		} else {
			console.log("Is not web.");
			this.bindEvents();
		}
	},
	bindEvents : function() {
		document.addEventListener('deviceready', this.onDeviceReady, false);
	},
	onDeviceReady : function() {
		// The scope of 'this' is the event, hence we need to use app.
		app.receivedEvent('deviceready');
	},
	receivedEvent : function(event) {
		switch (event) {
		case 'deviceready':
			pgReady.resolve();
			break;
		}
	}
};

$(document).on("pageinit", function(event, ui) {
	jqmReady.resolve();
});
/**
 * General initialization.
 */
$.when(jqmReady, pgReady).then(function() {
	// Initialization code here
	if (app.callback) {
		console.log("Calling Callback.");
		app.callback();
	}
	console.log("Frameworks ready.");
});



app.initialize(function() {
	console.log("Inicializando.");
    
	var defaultLatLng = new google.maps.LatLng(34.0983425, -118.3267434);  // Default to Hollywood, CA when no geolocation support
	navigator.geolocation.getCurrentPosition(drawMap, onError);
	
	function onError(error) {
	    console.log('code: '    + error.code    + '\n' +
		          'message: ' + error.message + '\n');
	    drawMap(defaultLatLng);  //show default map
	}
	
    function drawMap(position) {
    	console.log("drawMap.");
		var latlng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
        var myOptions = {
            zoom: 10,
            center: latlng,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        var map = new google.maps.Map(document.getElementById("map-canvas"), myOptions);
        // Add an overlay to the map of current lat/lng
        
        var marker = new google.maps.Marker({
            position: latlng,
            map: map,
            title: "Parqueadero"
        });
        
        var infowindow = new google.maps.InfoWindow({
      	  	content:"<div><img class='.media-object' /></div>"
        });

        $('#map-canvas').on('taphold', function(event){
        	infowindow.open(map, marker);
        	takePicture(event);
        });

        
        google.maps.event.addListener(marker, 'click', function() {
        	  infowindow.open(map, marker);
        });

        

        
        

    }
    function takePicture(event){
    	  event.preventDefault();
    	  if (!navigator.camera) {
    	      alert("Camera API not supported", "Error");
    	      return;
    	  }
    	  var options =   {   quality: 50,
    	                      destinationType: Camera.DestinationType.DATA_URL,
    	                      sourceType: 1,      // 0:Photo Library, 1=Camera, 2=Saved Album
    	                      encodingType: 0     // 0=JPG 1=PNG
    	                  };

    	  navigator.camera.getPicture(
    	      function(imgData) {
    	          $('.media-object').attr('src', "data:image/jpeg;base64,"+imgData);
    	          
    	      },
    	      function() {
    	          alert('Error taking picture', 'Error');
    	      },
    	      options);

    	  return false;
    }
});