package co.com.eafit.topicos.biciparqueo;

public class Punto {
	Double latitud, longitud;
	
	public Punto() {
	}

	public Punto(double i, double j) {
		latitud = Double.valueOf(i);
		longitud = Double.valueOf(j);
	}

	public Double getLatitud() {
		return latitud;
	}

	public void setLatitud(Double latitud) {
		this.latitud = latitud;
	}

	public Double getLongitud() {
		return longitud;
	}

	public void setLongitud(Double longitud) {
		this.longitud = longitud;
	}
}
