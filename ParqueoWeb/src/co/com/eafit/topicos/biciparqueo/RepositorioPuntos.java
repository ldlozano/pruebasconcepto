package co.com.eafit.topicos.biciparqueo;

import java.util.ArrayList;
import java.util.List;

public class RepositorioPuntos {
	
	private static RepositorioPuntos instance;
	private List<Punto> cache;
	
	private RepositorioPuntos(){
		inicializar();
	}


	private void inicializar(){
		if(cache == null){
			cache = new ArrayList<Punto>();
		}
		
		cache.add(new Punto(6.2001,-75.1788));
		cache.add(new Punto(6.0001,-75.2788));
		cache.add(new Punto(6.7001,-75.3788));
		cache.add(new Punto(6.1001,-75.4788));
		cache.add(new Punto(6.9001,-75.5788));
		cache.add(new Punto(6.8001,-75.6788));
		cache.add(new Punto(6.4001,-75.7788));
		cache.add(new Punto(6.3001,-75.8788));
	}
	
	public static RepositorioPuntos getInstance(){
		if(instance == null){
			instance = new RepositorioPuntos();
		}
		return instance;
	}
	
	public List<Punto> getPuntos(){
		return cache;
	}
	
	
	
}
