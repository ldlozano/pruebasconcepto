package co.com.eafit.topicos.biciparqueo;

import java.util.ArrayList;
import java.util.Collection;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;


@Path("/api")
public class ServiciosRest {
	
	@POST
	@Path("/calcularpuntos")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Collection<Punto> calcularPuntos(Punto origen){
		ArrayList retorno = new ArrayList(RepositorioPuntos.getInstance().getPuntos());
		return retorno;
	}
}
