package co.com.eafit.topicos.biciparqueo;

import java.util.ArrayList;
import java.util.List;

public class RepositorioPuntos {
	private static RepositorioPuntos instance;
	private List<Punto> puntos;
	
	private RepositorioPuntos(){
		inicializarPuntos();
	}
	
	private void inicializarPuntos() {
		puntos = new ArrayList<Punto>();
		puntos.add(new Punto(6.200333, -75.578205));
		puntos.add(new Punto(6.200927, -75.579000));
		puntos.add(new Punto(6.200353, -75.579177));
		puntos.add(new Punto(6.198469, -75.578278));
		puntos.add(new Punto(6.198875, -75.578938));
		puntos.add(new Punto(6.201499, -75.577986));
		puntos.add(new Punto(6.197488, -75.579949));
	}

	public static RepositorioPuntos getInstance(){
		if(instance == null){
			instance = new RepositorioPuntos();
		}
		return instance;
	}
	
	public ArrayList<Punto> getPuntos(){
		return (ArrayList<Punto>) puntos;
	}
	
}
