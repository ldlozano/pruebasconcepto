package co.com.eafit.topicos.biciparqueo;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;


@Path("/api")
public class ServiciosRest {
	
	@POST
	@Path("/calcularpuntos")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public List<Punto> calcularPuntos(Punto origen){
		System.out.println("Recibiendo punto " + origen.getLatitud() + " - " + origen.getLongitud());
		RepositorioPuntos repo = RepositorioPuntos.getInstance();
		List<Punto> retorno = new ArrayList<Punto>(repo.getPuntos());
		return retorno; 
	}
}
