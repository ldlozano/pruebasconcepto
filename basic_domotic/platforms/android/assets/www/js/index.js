/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

var glatitud, glongitud, galtitud;
var	clatitud, clongitud, caltitud;
var	stopCompare;


var app = {
    // Application Constructor
    initialize: function() {
        this.bindEvents();
    },
    // Bind Event Listeners
    //
    // Bind any events that are required on startup. Common events are:
    // 'load', 'deviceready', 'offline', and 'online'.
    bindEvents: function() {
        document.addEventListener('deviceready', this.onDeviceReady, false);
    },
    // deviceready Event Handler
    //
    // The scope of 'this' is the event. In order to call the 'receivedEvent'
    // function, we must explicitly call 'app.receivedEvent(...);'
    onDeviceReady: function() {
    	console.log("Dispositivo Listo.");
    	
    	
    	
    	$(document).on("pageinit", "#set_location", function(e) {
    		
        	$("#getGPSData").on("tap", function() {
        		console.log("Consultando GPS");
        		e.preventDefault();
      		  	updatePosition();
        	});      
        	$("#saveGPSData").on("tap", function() {
        		console.log("Guradando Posicion GPS");
        		e.preventDefault();
      		  	savePosition();
      		    poolPosition();
        	});      
        });
    	
    	if (navigator.notification) { // Override default HTML alert with native dialog
    	      window.alert = function (message) {
    	          navigator.notification.alert(
    	              message,    // message
    	              null,       // callback
    	              "Open Doors", // title
    	              'OK'        // buttonName
    	          );
    	      };
    	}
    	
    	function poolPosition(){
    		console.log("Iniciando Pooling de GPS");
    		stopCompare = true;
    		navigator.geolocation.watchPosition(comparePosition);
    	}
    	
    	function comparePosition(position){
    	 console.log("Se movio de posición");
   		 clatitud = position.coords.latitude;
		 clongitud = position.coords.longitude;
		 caltitud = position.coords.altitude;
		 $(".status").html("Tu posición actual es: \n LAT:" + clatitud + "\n LONG:" + clongitud + " \n ALT: " + caltitud);
		 stopCompare = true;
    	 if((glatitud === clatitud)&&(glongitud === clongitud) && stopCompare){
    		 stopCompare = false;
    		 alert('Estas cerca de tu casa. Liberar esquema de seguridad?');
    	 }else{
    		 alert('Saliste de tu casa. Activar esquema de seguridad?');
    	 }
    	}
    	
    	function updatePosition(){
    		navigator.geolocation.getCurrentPosition(onGPSSuccess, onGPSError);
   		 	console.log("Actualizando posicion");
    	}
    	
    	function savePosition(){
    		glatitud = clatitud;
    		glongitud = clongitud;
    		galtitud = caltitud;
    	}
    	
    	function onGPSSuccess(position)  {
    		 clatitud = position.coords.latitude;
    		 clongitud = position.coords.longitude;
    		 caltitud = position.coords.altitude
    		 fechaAct = new Date(position.timestamp);
    		 $(".current_location").html("Tu posición actual es: \n LAT:" + clatitud + "\n LONG:" + clongitud + " \n ALT: " + caltitud);
    	}
    	
    	
    	function onGPSError(error)  { alert('code: ' +error.code+ '\n' +  'message: ' +error.message+ '\n');  }
    },
};
