package com.eafit.conferre.conferencias.api;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.soluciones.conferre.conferencia.core.casosuso.CasoUso;
import com.soluciones.conferre.conferencia.core.entidades.Conferencia;

@Component
@Path("/conferencias")
public class ConferenciaService {

	@Autowired
	private CasoUso<String, Conferencia> crearConferenciaCasoUso; 
	
	@Path("/{id}")
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	public Response crearConferencia(Conferencia conferencia){
		String retorno = crearConferenciaCasoUso.ejecutar(conferencia);
		return Response.ok("{ \"conferenciaId\":" + retorno + "}").build();
	}
	
	
}
