package com.eafit.conferre.conferencia.test.selenium;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.net.URL;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

public class Escenario01Test {
  private WebDriver driver;
  private String baseUrl;
  private boolean acceptNextAlert = true;
  private StringBuffer verificationErrors = new StringBuffer();

  @Before
  public void setUp() throws Exception {
	DesiredCapabilities capability = DesiredCapabilities.firefox();

    driver = new RemoteWebDriver(new URL("http://localhost:4444/wd/hub"), capability);
    baseUrl = "http://tomcatcont:8081/";
    driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
    driver.manage().window().setSize(new Dimension(1920, 1080));
  }

  @Test
  public void testEscenario01() throws Exception {
    driver.get(baseUrl + "/conferre_jsf-0.0.1-SNAPSHOT/crearConferencia.xhtml");
    driver.findElement(By.id("j_idt5:j_idt11")).clear();
    driver.findElement(By.id("j_idt5:j_idt11")).sendKeys("Conferencia 1");
    driver.findElement(By.xpath("//div[@id='j_idt5:j_idt13']/div[2]/ul/li[2]")).click();
    driver.findElement(By.id("j_idt5:j_idt18_input")).click();
    driver.findElement(By.linkText("20")).click();
    driver.findElement(By.cssSelector("html")).click();
    driver.findElement(By.id("j_idt5:j_idt20_input")).click();
    driver.findElement(By.linkText("20")).click();
    driver.findElement(By.cssSelector("html")).click();
    driver.findElement(By.id("j_idt5:j_idt6_next")).click();
    driver.findElement(By.id("j_idt5:j_idt25")).click();
    driver.findElement(By.id("j_idt5:j_idt29")).clear();
    driver.findElement(By.id("j_idt5:j_idt29")).sendKeys("Evento 1");
    driver.findElement(By.id("j_idt5:j_idt31_input")).click();
    driver.findElement(By.xpath("//div[@id='j_idt5:j_idt26']/div[2]/table/tbody/tr[3]/td")).click();
    driver.findElement(By.id("j_idt5:duracion")).clear();
    driver.findElement(By.id("j_idt5:duracion")).sendKeys("2");
    driver.findElement(By.id("j_idt5:j_idt34")).click();
    driver.findElement(By.cssSelector("span.ui-icon.ui-icon-closethick")).click();
    driver.findElement(By.id("j_idt5:j_idt41")).click();
    driver.findElement(By.id("j_idt5:j_idt6_back")).click();
    driver.findElement(By.xpath("//div[@id='j_idt5:growl_container']/div/div/div")).click();
    driver.findElement(By.id("j_idt5:j_idt6_back")).click();
    driver.findElement(By.id("j_idt5:j_idt18_input")).click();
    driver.findElement(By.linkText("21")).click();
    driver.findElement(By.cssSelector("html")).click();
    driver.findElement(By.id("j_idt5:j_idt20_input")).click();
    driver.findElement(By.linkText("21")).click();
    driver.findElement(By.cssSelector("html")).click();
    driver.findElement(By.id("j_idt5:j_idt6_next")).click();
    driver.findElement(By.id("j_idt5:j_idt41")).click();
    assertEquals("Se ha creado la conferencia :bad10434-099a-4063-aaec-5c5a89d9817e", driver.findElement(By.cssSelector("p")).getText());
  }

  @After
  public void tearDown() throws Exception {
    driver.quit();
    String verificationErrorString = verificationErrors.toString();
    if (!"".equals(verificationErrorString)) {
      fail(verificationErrorString);
    }
  }

  private boolean isElementPresent(By by) {
    try {
      driver.findElement(by);
      return true;
    } catch (NoSuchElementException e) {
      return false;
    }
  }

  private boolean isAlertPresent() {
    try {
      driver.switchTo().alert();
      return true;
    } catch (NoAlertPresentException e) {
      return false;
    }
  }

  private String closeAlertAndGetItsText() {
    try {
      Alert alert = driver.switchTo().alert();
      String alertText = alert.getText();
      if (acceptNextAlert) {
        alert.accept();
      } else {
        alert.dismiss();
      }
      return alertText;
    } finally {
      acceptNextAlert = true;
    }
  }
}
