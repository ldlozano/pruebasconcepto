package com.soluciones.conferre.conferencia.core.casosuso;

public interface CasoUso<Resultado, Parametros> {
	
	public Resultado ejecutar(Parametros parametros);
}
