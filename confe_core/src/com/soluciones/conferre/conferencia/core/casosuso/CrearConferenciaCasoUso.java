package com.soluciones.conferre.conferencia.core.casosuso;

import com.soluciones.conferre.conferencia.core.entidades.Conferencia;
import com.soluciones.conferre.conferencia.core.repositorio.ConferenciasRepositorio;
import com.soluciones.conferre.conferencia.core.util.exception.CasoUsoException;

public class CrearConferenciaCasoUso implements CasoUso<String, Conferencia> {

	private ConferenciasRepositorio repositorio;

	public CrearConferenciaCasoUso(ConferenciasRepositorio repositorio) {
		this.repositorio = repositorio;
	}
	
	@Override
	public String ejecutar(Conferencia conferencia) {
		String retorno;
		if(conferencia.validarFechas()){
			repositorio.nuevaConferencia(conferencia);
			retorno = conferencia.getId().toString();
		}else{
			throw new CasoUsoException("No se ha creado la conferencia, fechas invalidas");
		}
		
		return retorno;
	}

}
