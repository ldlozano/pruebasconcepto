/*
 * Copyright Lenin Lozano 2015
 */
package com.soluciones.conferre.conferencia.core.entidades;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

/*
 * Entidad que contiene la logica de negocio alrededor de las conferencias.
 * @author ldlozano
 * @since version 0.0.1
 */
public class Conferencia {
	private UUID id; //UUID
	private String nombre;
	private List<Evento> eventos;
	private String lugar;
	private Date fechaInicio;
	private Date fechaFinalizacion;
	
	public Conferencia(String nombre, Date fechaInicio, Date fechaFinalizacion) {
		id = UUID.randomUUID();
		this.nombre = nombre;
		this.fechaInicio = fechaInicio;
		this.fechaFinalizacion = fechaFinalizacion;
		eventos = new ArrayList<Evento>();
	}
	
	public UUID getId() {
		return id;
	}
	public void setId(UUID id) {
		this.id = id;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	public List<Evento> getEventos() {
		
		return eventos;
	}
	public void setEventos(List<Evento> eventos) {
		this.eventos = eventos;
	}
	
	public void addEventos(Evento evento){
		if(evento.validarDuracion(this.getFechaInicio(), this.getFechaFinalizacion())){
			this.eventos.add(evento);
		}
	}
	
	
	public String getLugar() {
		return lugar;
	}
	public void setLugar(String lugar) {
		this.lugar = lugar;
	}
	
	
	public Date getFechaInicio() {
		return fechaInicio;
	}
	public void setFechaInicio(Date fechaInicio) {
		this.fechaInicio = fechaInicio;
	}
	public Date getFechaFinalizacion() {
		return fechaFinalizacion;
	}
	public void setFechaFinalizacion(Date fechaFinalizacion) {
		this.fechaFinalizacion = fechaFinalizacion;
	}
	
	@Override
	public boolean equals(Object obj) {
		if(obj == null)
			return false;
		
		Conferencia comparado = (Conferencia) obj;
		if(this.id.equals(comparado.getId()))
		{
			return true;
		}
		
		return false;
	}
	
	@Override
	public int hashCode() {
		return 1;
	}
	
	/***REGLAS DE NEGOCIO DE LA CONFERENCIA***/
	
	public boolean validarFechas(){
		if(fechaInicio.after(fechaFinalizacion)){
			return false;
		}
		if(fechaInicio.before(new Date(System.currentTimeMillis()))){
			return false;
		}
		if(fechaFinalizacion.before(new Date(System.currentTimeMillis()))){
			return false;
		}
		return true;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
