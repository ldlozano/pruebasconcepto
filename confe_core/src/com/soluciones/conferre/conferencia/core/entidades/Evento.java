package com.soluciones.conferre.conferencia.core.entidades;

import java.util.Date;
import java.util.List;
import java.util.UUID;

/**
 * Define los multiples eventos que se realizarán en una conferencia
 * @author david
 * @since 0.0.1
 */
public class Evento {
	private UUID id;
	private String nombre;
	private Date horaInicio;
	private int duracionEnHoras;
	private List<Silla> ubicaciones;

	public Evento(String nombre, Date horaInicio, int duracionEnHoras) {
		id = UUID.randomUUID();
		this.horaInicio = horaInicio;
		this.duracionEnHoras = duracionEnHoras;
	}
	
	public UUID getId() {
		return id;
	}
	public void setId(UUID id) {
		this.id = id;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public Date getHoraInicio() {
		return horaInicio;
	}
	public void setHoraInicio(Date horaInicio) {
		this.horaInicio = horaInicio;
	}
	public int getDuracionEnHoras() {
		return duracionEnHoras;
	}
	public void setDuracionEnHoras(int duracionEnHoras) {
		this.duracionEnHoras = duracionEnHoras;
	}
	public List<Silla> getUbicaciones() {
		return ubicaciones;
	}
	public void setUbicaciones(List<Silla> ubicaciones) {
		this.ubicaciones = ubicaciones;
	}
	
	@Override
	public boolean equals(Object obj) {
		if(obj == null){
			return false;
		}
		
		Evento eventoAComparar = (Evento) obj;
		if(this.getId().equals(eventoAComparar.getId())){
			return true;
		}
		
		return false;		
	}
	
	@Override
	public int hashCode() {
		return 1;
	}

	public boolean validarDuracion(Date fechaInicio, Date fechaFinalizacion) {
		return true;
	}
	
}
