package com.soluciones.conferre.conferencia.core.entidades;

import com.soluciones.conferre.conferencia.core.util.EstadoSilla;
import com.soluciones.conferre.conferencia.core.util.TipoSilla;

public class Silla {
	private String id;
	private double precio;
	private TipoSilla tipoSilla;
	private EstadoSilla estado;
	
	public Silla(String id, double precio, TipoSilla tipo) {
		this.id = id;
		this.precio = precio;
		this.tipoSilla = tipo;
		this.setEstado(EstadoSilla.DISPONIBLE);
	}
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public double getPrecio() {
		return precio;
	}
	public void setPrecio(double precio) {
		this.precio = precio;
	}
	public TipoSilla getTipoSilla() {
		return tipoSilla;
	}
	public void setTipoSilla(TipoSilla tipoSilla) {
		this.tipoSilla = tipoSilla;
	}

	public EstadoSilla getEstado() {
		return estado;
	}

	public void setEstado(EstadoSilla estado) {
		this.estado = estado;
	}
	
	@Override
	public boolean equals(Object obj) {
		if(obj == null)
			return false;
		Silla sillaComparada = (Silla) obj;
		if(sillaComparada.getId().isEmpty())
			return false;
		return this.getId().equals(sillaComparada.getId());
	}
	
	@Override
	public int hashCode() {
		return 1;
	}
	
}
