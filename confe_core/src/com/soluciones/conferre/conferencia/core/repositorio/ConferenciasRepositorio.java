package com.soluciones.conferre.conferencia.core.repositorio;

import com.soluciones.conferre.conferencia.core.entidades.Conferencia;

public interface ConferenciasRepositorio {
	public int nuevaConferencia(Conferencia conferencia);
}
