package com.soluciones.conferre.conferencia.core.util;

public enum EstadoSilla {
	DISPONIBLE, VENDIDA, RESERVADA;
}
