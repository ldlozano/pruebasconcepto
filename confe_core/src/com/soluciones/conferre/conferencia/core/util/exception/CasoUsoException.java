package com.soluciones.conferre.conferencia.core.util.exception;

public class CasoUsoException extends RuntimeException {
	public CasoUsoException(String message) {
		super(message);
	}
}
