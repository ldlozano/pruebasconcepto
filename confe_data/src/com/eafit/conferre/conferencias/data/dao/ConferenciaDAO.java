package com.eafit.conferre.conferencias.data.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.eafit.conferre.conferencias.data.entidades.ConferenciaEntity;

public interface ConferenciaDAO extends JpaRepository<ConferenciaEntity, String> {

}
