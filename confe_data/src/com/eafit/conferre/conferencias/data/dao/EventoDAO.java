package com.eafit.conferre.conferencias.data.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.eafit.conferre.conferencias.data.entidades.EventoEntity;
import com.eafit.conferre.conferencias.data.entidades.EventoKey;

public interface EventoDAO extends JpaRepository<EventoEntity, EventoKey> {

}
