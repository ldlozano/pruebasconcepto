package com.eafit.conferre.conferencias.data.dao;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;

import com.eafit.conferre.conferencias.data.entidades.SillaEntity;
import com.eafit.conferre.conferencias.data.entidades.SillaKey;

public interface SillaDAO extends JpaRepository<SillaEntity, SillaKey>{

}
