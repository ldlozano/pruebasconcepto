package com.eafit.conferre.conferencias.data.entidades;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.springframework.beans.BeanUtils;

import com.soluciones.conferre.conferencia.core.entidades.Conferencia;
import com.soluciones.conferre.conferencia.core.entidades.Evento;

/**
 * Entity implementation class for Entity: ConferenciaEntity
 *
 */
@Entity
@Table(name="conferencias")

public class ConferenciaEntity implements Serializable {

	   
	@Id
	private String id;
	private String nombre;
	private String lugar;
	@Column(name = "fecha_inicio")
	private Timestamp fechaInicio;
	@Column(name = "fecha_fin")
	private Timestamp fechaFin;
	
	@OneToMany(mappedBy="conferencia", cascade=CascadeType.ALL)
	private List<EventoEntity> eventos;
	
	private static final long serialVersionUID = 1L;

	public ConferenciaEntity() {
		super();
	}   
	
	public ConferenciaEntity(Conferencia conferencia) {
		this.id = conferencia.getId().toString();
		
		BeanUtils.copyProperties(conferencia, this, "eventos");
		List<Evento> eventosBO = conferencia.getEventos();
		if(!eventosBO.isEmpty()){
			eventos = new ArrayList<EventoEntity>();
			for (Evento evento : eventosBO) {
				EventoEntity newEventoEntity = new EventoEntity(evento);
				this.eventos.add(newEventoEntity);
			}
		}
		
	}



	public String getId() {
		return this.id;
	}

	public void setId(String id) {
		this.id = id;
	}   
	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}   
	public String getLugar() {
		return this.lugar;
	}

	public void setLugar(String lugar) {
		this.lugar = lugar;
	}   
	public Timestamp getFechaInicio() {
		return this.fechaInicio;
	}

	public void setFechaInicio(Timestamp fechaInicio) {
		this.fechaInicio = fechaInicio;
	}   
	public Timestamp getFechaFin() {
		return this.fechaFin;
	}

	public void setFechaFin(Timestamp fechaFin) {
		this.fechaFin = fechaFin;
	}

	public List<EventoEntity> getEventos() {
		return eventos;
	}

	public void setEventos(List<EventoEntity> eventos) {
		this.eventos = eventos;
	}
   
}
