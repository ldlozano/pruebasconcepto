package com.eafit.conferre.conferencias.data.entidades;

import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.springframework.beans.BeanUtils;

import com.soluciones.conferre.conferencia.core.entidades.Evento;
import com.soluciones.conferre.conferencia.core.entidades.Silla;

@Entity
@Table(name="eventos")
public class EventoEntity {
	
	@Id
	private String id;
	
	@ManyToOne
	private ConferenciaEntity conferencia;
	
	private String nombre;
	
	@Column(name="hora_inicio")
	private Timestamp horaInicio;
	
	private int duracion;
	
	public EventoEntity() {
	}
	
	public EventoEntity(Evento evento) {
		this.id = evento.getId().toString();
		BeanUtils.copyProperties(evento, this, "ubicaciones");
		/*List<Silla> ubicaciones = evento.getUbicaciones();
		if(!ubicaciones.isEmpty()){
			for (Silla silla : ubicaciones) {
				SillaEntity newSillaEntity = new SillaEntity(silla);
				this.sillas.add(newSillaEntity);
			}
		}*/
	}
	
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public Timestamp getHoraInicio() {
		return horaInicio;
	}
	public void setHoraInicio(Timestamp horaInicio) {
		this.horaInicio = horaInicio;
	}
	public int getDuracion() {
		return duracion;
	}
	public void setDuracion(int duracion) {
		this.duracion = duracion;
	}
	
	
}
