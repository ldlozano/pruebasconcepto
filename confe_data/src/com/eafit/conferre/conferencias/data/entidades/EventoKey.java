package com.eafit.conferre.conferencias.data.entidades;

import java.io.Serializable;

import javax.persistence.ManyToOne;

public class EventoKey implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 469619773401949155L;
	private String conferenciaId;
	private String id;
	

	public String getConferenciaId() {
		return conferenciaId;
	}

	public void setConferenciaId(String conferenciaId) {
		this.conferenciaId = conferenciaId;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	
	@Override
	public boolean equals(Object obj) {
		if(obj == null)
			return false;
		EventoKey key = (EventoKey) obj;
		if(key.getId().equals(this.id) && key.getConferenciaId().equals(this.conferenciaId)){
			return true;
		}
		return false;
	}

	@Override
	public int hashCode() {
		int hash = 1;
        hash = hash * 31 + id.hashCode();
        hash = hash * 13 + conferenciaId.hashCode();
        return hash;
	}
	
	
}
