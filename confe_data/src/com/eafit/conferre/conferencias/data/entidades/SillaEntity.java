package com.eafit.conferre.conferencias.data.entidades;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.beans.BeanUtils;

import com.soluciones.conferre.conferencia.core.entidades.Silla;

@Entity
@Table(name="sillas")
public class SillaEntity {
	@Id
	private String id;
	
	@Column
	private int estado;
	@Column
	private double precio;
	@Column
	private int tipoSilla;
	
	public SillaEntity() {
	}
	
	public SillaEntity(Silla silla) {
		BeanUtils.copyProperties(silla, this);
	}
	
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public int getEstado() {
		return estado;
	}
	public void setEstado(int estado) {
		this.estado = estado;
	}
	public double getPrecio() {
		return precio;
	}
	public void setPrecio(double precio) {
		this.precio = precio;
	}
	public int getTipoSilla() {
		return tipoSilla;
	}
	public void setTipoSilla(int tipoSilla) {
		this.tipoSilla = tipoSilla;
	}
	
	
	
}
