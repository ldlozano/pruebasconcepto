package com.eafit.conferre.conferencias.data.entidades;

import java.io.Serializable;

import javax.persistence.ManyToOne;


public class SillaKey implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String conferenciaId;
	private String eventoId;
	private String id;
	
	public String getEventoId() {
		return eventoId;
	}
	public void setEventoId(String eventoId) {
		this.eventoId = eventoId;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}

	@Override
	public boolean equals(Object obj) {
		if(obj == null)
			return false;
		SillaKey key = (SillaKey) obj;
		if(key.getId().equals(this.id) 
				&& key.getEventoId().equals(this.eventoId)){
			return true;
		}
		return false;
	}
	
	@Override
	public int hashCode() {
		int hash = 1;
        hash = hash * 31 + id.hashCode();
        hash = hash * 97 + eventoId.hashCode();
        return hash;
	}
	
}
