package com.eafit.conferre.conferencias.data.repositorio;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.soluciones.conferre.conferencia.core.entidades.Conferencia;
import com.soluciones.conferre.conferencia.core.repositorio.ConferenciasRepositorio;

@Repository(value="enMemoriaConferenciasRepositorio")
public class EnMemoriaConferenciasRepositorio implements
		ConferenciasRepositorio {

	private Map<String, Conferencia> conferencias;
	
	
	@Override
	public int nuevaConferencia(Conferencia conferencia) {
		if(conferencias == null){
			conferencias = new HashMap<String, Conferencia>();
		}
		conferencias.put(conferencia.getId().toString(), conferencia);
		return 1;
	}

}
