package com.eafit.conferre.conferencias.data.repositorio;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.eafit.conferre.conferencias.data.dao.ConferenciaDAO;
import com.eafit.conferre.conferencias.data.entidades.ConferenciaEntity;
import com.soluciones.conferre.conferencia.core.entidades.Conferencia;
import com.soluciones.conferre.conferencia.core.repositorio.ConferenciasRepositorio;

@Repository(value="hibernateConferenciaRepositorio")
public class HibernateConferenciasRepositorio implements
		ConferenciasRepositorio  {

	@Autowired
	private ConferenciaDAO conferenciaDAO;
	
	@Override
	public int nuevaConferencia(Conferencia conferencia) {
		
		ConferenciaEntity conferenciaEntity = new ConferenciaEntity(conferencia);
		conferenciaDAO.save(conferenciaEntity);
		return 0;
	}
	
}
