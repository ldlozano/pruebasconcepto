package com.eafit.conferre.conferencias.web.beans;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import org.primefaces.event.FlowEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.soluciones.conferre.conferencia.core.casosuso.CasoUso;
import com.soluciones.conferre.conferencia.core.entidades.Conferencia;
import com.soluciones.conferre.conferencia.core.entidades.Evento;

@Component
@ManagedBean(name = "crearConferenciaWizard")
@ViewScoped
public class CrearConferenciaWizard {
	Logger logger = LoggerFactory.getLogger(this.getClass().getCanonicalName());

	private Conferencia conferencia;
	private Evento evento = new Evento(null, null, 0);

	@PostConstruct
	public void reset() {
		logger.debug("Reiniciando!!!");
		conferencia = new Conferencia(null, null, null);
	}

	@PreDestroy
	public void finish() {
		logger.debug("finishing view");
	}

	public Conferencia getConferencia() {
		return conferencia;
	}

	public void setConferencia(Conferencia conferencia) {
		this.conferencia = conferencia;
	}

	public Evento getEvento() {
		return evento;
	}

	public void setEvento(Evento evento) {
		this.evento = evento;
	}

	@Autowired
	CasoUso<String, Conferencia> crearConferenciaCasoUso;

	public String onFlowProcess(FlowEvent event) {
		logger.info("Current wizard step:" + event.getOldStep());
		logger.info("Next step:" + event.getNewStep());
		return event.getNewStep();
	}

	public void save() {
		FacesMessage msg = null;
		try {
			String id = crearConferenciaCasoUso.ejecutar(conferencia);
			msg = new FacesMessage("Exitoso", "Se ha creado la conferencia :"
					+ id);
		} catch (Exception e) {
			e.printStackTrace();
			msg = new FacesMessage("Problemas", e.getMessage());
		}
		FacesContext.getCurrentInstance().addMessage(null, msg);
	}

	public void addEvento() {
		logger.info("Adicionando evento " + evento.getNombre());
		conferencia.addEventos(evento);
	}

	public void nuevoEvento() {
		evento = new Evento(null, null, 0);
		logger.info("Nuveo evento: " + evento.getId().toString());
	}

}
