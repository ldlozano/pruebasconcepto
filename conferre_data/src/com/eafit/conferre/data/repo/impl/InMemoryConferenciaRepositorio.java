package com.eafit.conferre.data.repo.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.eafit.conferre.conferencias.entities.Conferencia;
import com.eafit.conferre.conferencias.repositories.ConferenciasRepositorio;
import com.eafit.conferre.conferencias.soporte.dto.ConferenciaDTO;

public class InMemoryConferenciaRepositorio implements ConferenciasRepositorio {

	private List<Conferencia> conferencias;
	
	public InMemoryConferenciaRepositorio() {
		conferencias = new ArrayList<Conferencia>();
		conferencias.add(new Conferencia());
	}
	
	@Override
	public List<Conferencia> consultarConferencias(Date fechaInicio,
			Date fechaFin) {
		
		
		return null;
	}

	@Override
	public Conferencia getConferencia(String id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void guardar(Conferencia conferencia) {
		// TODO Auto-generated method stub
		
	}

}
