package com.eafit.conferre.soporte;

import com.eafit.conferre.conferencias.repositories.ConferenciasRepositorio;
import com.eafit.conferre.data.repo.impl.InMemoryConferenciaRepositorio;

public class ServiceLocator {
	ConferenciasRepositorio repo;
	
	private static ServiceLocator instance;
	
	private ServiceLocator() {
		repo = new InMemoryConferenciaRepositorio();
	}
	
	public static ServiceLocator getInstance(){
		if(instance == null){
			instance = new ServiceLocator();
		}
		
		return instance;
	}
	
	public ConferenciasRepositorio getConferenciasRepo(){
		return repo;
	}
	
}
