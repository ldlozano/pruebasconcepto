package com.conferre.conferencias.test.acceptance;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import cucumber.api.java.After;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class CrearConferenciaTest {
	  private WebDriver driver;
	  private String baseUrl;
	  private boolean acceptNextAlert = true;
	  private StringBuffer verificationErrors = new StringBuffer();
	

	@Given("^que estoy autenticado en la aplicacion$")
	public void que_estoy_autenticado_en_la_aplicacion() throws Throwable {
	    driver = new FirefoxDriver();
	    baseUrl = "https://www.google.com.co/";
	    driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	    driver.get(baseUrl + "/?gfe_rd=cr&ei=BvyNVculPOut8wfK44OwAg&gws_rd=ssl");
	}

	@Given("^Ingreso los datos validos de la conferencia$")
	public void ingreso_los_datos_validos_de_la_conferencia() throws Throwable {
	    driver.findElement(By.id("lst-ib")).clear();
	    driver.findElement(By.id("lst-ib")).sendKeys("Colombia vs argentina 2015");
	}

	@Given("^creo un evento para la conferencia$")
	public void creo_un_evento_para_la_conferencia() throws Throwable {
	}

	@When("^hago click en Crear Conferencia$")
	public void hago_click_en_Crear_Conferencia() throws Throwable {
	    driver.findElement(By.name("btnG")).click();
	}

	@Then("^la conferencia debe ser creada$")
	public void la_conferencia_debe_ser_creada() throws Throwable {
	    try {
	        assertTrue(isElementPresent(By.linkText("Copa Am�rica: Previa: Colombia vs. Argentina - Copa ...")));
	      } catch (Error e) {
	        verificationErrors.append(e.toString());
	      }
	}

	@Then("^se debe mostrar el id de la conferencia al coordinador$")
	public void se_debe_mostrar_el_id_de_la_conferencia_al_coordinador() throws Throwable {
	}

	
	  private boolean isElementPresent(By by) {
		    try {
		      driver.findElement(by);
		      return true;
		    } catch (NoSuchElementException e) {
		      return false;
		    }
		  }

	  @After
	  public void tearDown() throws Exception {
	    driver.quit();
	    String verificationErrorString = verificationErrors.toString();
	    if (!"".equals(verificationErrorString)) {
	      fail(verificationErrorString);
	    }
	  }
	
	
}
