Feature: Realizar la creacion de conferencias
User Story: 
  As a coordinator
  I wish to create a conference
  To sell conferences online
  
Scenario: Crear una conferencia de 1 evento
  Given que estoy autenticado en la aplicacion
  And Ingreso los datos validos de la conferencia
  And creo un evento para la conferencia
  When hago click en Crear Conferencia
  Then la conferencia debe ser creada
  And se debe mostrar el id de la conferencia al coordinador
  
