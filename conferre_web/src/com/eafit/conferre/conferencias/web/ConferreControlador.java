package com.eafit.conferre.conferencias.web;

import com.eafit.conferre.conferencias.soporte.dto.SeleccionConferenciaDTO;
import com.eafit.conferre.conferencias.usecase.CasoUsoAsignarBoleta;
import com.eafit.conferre.soporte.ServiceLocator;

public class ConferreControlador {
	CasoUsoAsignarBoleta usecase = new CasoUsoAsignarBoleta(ServiceLocator.getInstance().getConferenciasRepo());
	
	public void handle(Request peticion){
		SeleccionConferenciaDTO dto = procesarPeticion(peticion);
		usecase.setParametros(dto);
		usecase.execute();
	}
	
}
