package co.com.eafit.topicos.biciparqueo;

import java.util.ArrayList;
import java.util.Collection;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;


@Path("/api")
public class ServiciosRest {
	
	@GET
	@Path("/calcularpuntos")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Collection<Punto> calcularPuntos(Punto origen){
		ArrayList retorno = new ArrayList();
		retorno.add(new Punto(0,0));
		return retorno;
	}
}
