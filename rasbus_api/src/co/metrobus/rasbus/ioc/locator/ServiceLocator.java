package co.metrobus.rasbus.ioc.locator;

import java.util.HashMap;
import java.util.Map;

import co.metrobus.rasbus.infra.repositories.HSQLDBRutaRepository;

public class ServiceLocator {
	private static ServiceLocator instance;
	
	private Map<String, Object> objetos;
	
	private ServiceLocator() {
		objetos = new HashMap<String, Object>();
		objetos.put("RutaRepository", new HSQLDBRutaRepository());
	}
	
	public static ServiceLocator getInstance(){
		if(instance == null){
			instance = new ServiceLocator();
		}
		return instance;
	}
	
	
	public Object getObject(String objectName){
		return objetos.get(objectName);
	}
	
	
	
	
}
