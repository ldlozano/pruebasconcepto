package co.metrobus.rasbus.ruta.controllers.rest;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import co.metrobus.rasbus.core.dto.RutaDTO;
import co.metrobus.rasbus.core.exception.UseCaseException;
import co.metrobus.rasbus.core.usecase.UseCase;
import co.metrobus.rasbus.ioc.locator.ServiceLocator;
import co.metrobus.rasbus.ruta.api.exception.APIException;
import co.metrobus.rasbus.ruta.entities.Posicion;
import co.metrobus.rasbus.ruta.entities.Ruta;
import co.metrobus.rasbus.ruta.entities.Tramo;
import co.metrobus.rasbus.ruta.repositories.RutaRepository;
import co.metrobus.rasbus.ruta.usecases.CrearRutaUseCase;

@Path("/rutas")
public class RestRutaController implements RutaController {

	
	@Path("/{rutaid}")
	@POST
	@Consumes("application/json")
	@Produces("application/json")
	public RutaDTO crearRuta(RutaDTO ruta){
		UseCase crearRutaCasoUso = 
				new CrearRutaUseCase((RutaRepository) ServiceLocator.getInstance().getObject("RutaRepository"));
		RutaDTO result = null;
		try{
			result = (RutaDTO) crearRutaCasoUso.execute(ruta);
		}catch(UseCaseException ex){
			throw new APIException("Error cuando se trataba de crear ruta");
			
		}
		return result;
	}
	
	@Path("/{id}")
	@GET
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public RutaDTO obtenerRuta(@PathParam("id") String rutaId)  {
		RutaDTO retorno = new RutaDTO();
		Posicion origen = new Posicion();
		Posicion destino = new Posicion();
		origen.setLatitud("1");
		origen.setLongitud("10");
		destino.setLatitud("10");
		destino.setLongitud("10");
		
		Tramo unico = new Tramo(origen, destino, 10);
		Ruta ruta = new Ruta(unico);
		retorno.setRuta(ruta);
		return retorno;
	}
	

}
