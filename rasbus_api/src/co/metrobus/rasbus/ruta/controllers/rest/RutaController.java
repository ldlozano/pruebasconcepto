package co.metrobus.rasbus.ruta.controllers.rest;

import co.metrobus.rasbus.core.dto.RutaDTO;

public interface RutaController {
	public RutaDTO crearRuta(RutaDTO ruta) throws Exception;
}
