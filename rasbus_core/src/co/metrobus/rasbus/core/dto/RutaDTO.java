package co.metrobus.rasbus.core.dto;

import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

import co.metrobus.rasbus.core.usecase.UseCaseParam;
import co.metrobus.rasbus.core.usecase.UseCaseResult;
import co.metrobus.rasbus.ruta.entities.Bus;
import co.metrobus.rasbus.ruta.entities.Ruta;
import co.metrobus.rasbus.ruta.entities.Tramo;

@XmlRootElement 
public class RutaDTO implements UseCaseResult, UseCaseParam {
	private Ruta ruta;

	public Ruta getRuta() {
		return ruta;
	}

	public void setRuta(Ruta ruta) {
		this.ruta = ruta;
	}
	
	public RutaDTO(Ruta ruta) {
		this.ruta = ruta;
	}

	public RutaDTO() {
		// TODO Auto-generated constructor stub
	}
}
