package co.metrobus.rasbus.core.exception;

public class UseCaseException extends RuntimeException {
	public UseCaseException(String message) {
		super(message);
	}
	
	public UseCaseException(Throwable e) {
		super(e);
	}
	
}
