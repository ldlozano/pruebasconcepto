package co.metrobus.rasbus.core.usecase;

import co.metrobus.rasbus.core.exception.UseCaseException;

public interface GenericUseCase<Result, Argument> {
	public Result execute(Argument parametro) throws UseCaseException;

}
