package co.metrobus.rasbus.core.usecase;

import co.metrobus.rasbus.core.dto.RutaDTO;
import co.metrobus.rasbus.core.exception.UseCaseException;

public interface UseCase {
	public UseCaseResult execute(UseCaseParam parametro) throws UseCaseException;
}
