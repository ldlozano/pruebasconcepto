package co.metrobus.rasbus.ruta.entities;

import java.util.List;

public class Bus {
	private String id;
	private String placa;
	private List<Posicion> recorrido;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getPlaca() {
		return placa;
	}
	public void setPlaca(String placa) {
		this.placa = placa;
	}
	
	public boolean validarDatos(){
		return true;
	}
	
	
	
}
