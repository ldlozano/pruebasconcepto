package co.metrobus.rasbus.ruta.repositories;

import co.metrobus.rasbus.core.exception.RepositoryException;
import co.metrobus.rasbus.ruta.entities.Ruta;

public interface RutaRepository {
	public Ruta adicionarNuevaRuta(Ruta ruta) throws RepositoryException;
	public Ruta obtenerRuta(Ruta parametro);
	public void actualizarRuta(Ruta ruta);
	
}
