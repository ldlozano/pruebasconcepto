package co.metrobus.rasbus.ruta.usecases;

import co.metrobus.rasbus.core.dto.RutaDTO;
import co.metrobus.rasbus.core.exception.RepositoryException;
import co.metrobus.rasbus.core.exception.UseCaseException;
import co.metrobus.rasbus.core.usecase.UseCase;
import co.metrobus.rasbus.core.usecase.UseCaseParam;
import co.metrobus.rasbus.core.usecase.UseCaseResult;
import co.metrobus.rasbus.ruta.entities.Ruta;
import co.metrobus.rasbus.ruta.repositories.RutaRepository;

public class CrearRutaUseCase implements UseCase {

	RutaRepository repo;
	
	
	public CrearRutaUseCase(RutaRepository repo) {
		this.repo = repo;
	}
	
	@Override
	public RutaDTO execute(UseCaseParam param) throws UseCaseException {
		RutaDTO rutaDto = (RutaDTO) param;
		Ruta ruta = rutaDto.getRuta();
		if(ruta.validarRuta()){
			try {
				repo.adicionarNuevaRuta(ruta);
			} catch (RepositoryException e) {
				throw new UseCaseException(e);
			}
		}
		return rutaDto;
	}

	
	
}
