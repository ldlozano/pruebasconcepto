package co.metrobus.rasbus.infra.dao.recorrido;

import java.util.List;

import co.metrobus.rasbus.ruta.entities.Ruta;

public interface RecorridoDAO {
	public Ruta create(String idRuta, String idTramo);
	public Ruta update(String idRuta, String idTramo);
	public Ruta retrieve(String idRuta, String idTramo);
	public List<Ruta> list();
	public int delete(String idRuta, String idTramo);
}
