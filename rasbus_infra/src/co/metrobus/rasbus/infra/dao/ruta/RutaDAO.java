package co.metrobus.rasbus.infra.dao.ruta;

import java.util.List;

import co.metrobus.rasbus.ruta.entities.Ruta;

public interface RutaDAO {
	public Ruta create(Ruta ruta);
	public Ruta update(Ruta ruta);
	public Ruta retrieve(Ruta ruta);
	public List<Ruta> list();
	public int delete(Ruta ruta);
}
