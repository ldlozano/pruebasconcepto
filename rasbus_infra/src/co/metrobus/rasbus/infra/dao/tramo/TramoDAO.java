package co.metrobus.rasbus.infra.dao.tramo;

import java.util.List;

import co.metrobus.rasbus.ruta.entities.Tramo;

public interface TramoDAO {
	public Tramo create(Tramo tramo);
	public Tramo update(Tramo tramo);
	public Tramo retrieve(Tramo tramo);
	public List<Tramo> list();
	public int delete(Tramo tramo);

}
