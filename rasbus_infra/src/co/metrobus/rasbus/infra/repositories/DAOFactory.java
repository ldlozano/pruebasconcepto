package co.metrobus.rasbus.infra.repositories;

import co.metrobus.rasbus.infra.dao.recorrido.RecorridoDAO;
import co.metrobus.rasbus.infra.dao.ruta.HSQLDBRutaDAO;
import co.metrobus.rasbus.infra.dao.ruta.InMemoryRutaDAO;
import co.metrobus.rasbus.infra.dao.ruta.RutaDAO;
import co.metrobus.rasbus.infra.dao.tramo.HSQLDBTramoDAO;
import co.metrobus.rasbus.infra.dao.tramo.InMemoryTramoDAO;
import co.metrobus.rasbus.infra.dao.tramo.TramoDAO;
import co.metrobus.rasbus.infra.util.TipoDB;

public class DAOFactory {
	private static DAOFactory instance;
	
	private RutaDAO rutaDAO;
	private TramoDAO tramoDAO;
	
	private DAOFactory() {
	}
	
	public static DAOFactory getInstance(){
		if(instance == null){
			instance = new DAOFactory();
		}
		return instance;
	}
	
	
	public RutaDAO getRutaDAO(TipoDB tipo){
		switch (tipo) {
		case HSQLDB:
			if(rutaDAO == null){
				rutaDAO = new HSQLDBRutaDAO();
			}
			break;
		default:
			if(rutaDAO == null){
				rutaDAO = new InMemoryRutaDAO();
			}
			break;
		}
		return rutaDAO;
	}

	public TramoDAO getTramoDAO(TipoDB tipo) {
		switch (tipo) {
		case HSQLDB:
			if(tramoDAO == null){
				tramoDAO = new HSQLDBTramoDAO();
			}
			break;
		default:
			if(tramoDAO == null){
				tramoDAO = new InMemoryTramoDAO();
			}
			break;
		}
		return tramoDAO;
	}

	public RecorridoDAO getRecorridoDAO(TipoDB tipo) {
		// TODO Auto-generated method stub
		return null;
	}
	
	
}
