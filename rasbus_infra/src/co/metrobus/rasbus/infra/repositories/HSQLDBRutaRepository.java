package co.metrobus.rasbus.infra.repositories;

import java.util.List;

import co.metrobus.rasbus.core.exception.RepositoryException;
import co.metrobus.rasbus.infra.dao.recorrido.RecorridoDAO;
import co.metrobus.rasbus.infra.dao.tramo.TramoDAO;
import co.metrobus.rasbus.infra.util.TipoDB;
import co.metrobus.rasbus.ruta.entities.Ruta;
import co.metrobus.rasbus.ruta.entities.Tramo;
import co.metrobus.rasbus.ruta.repositories.RutaRepository;


public class HSQLDBRutaRepository implements RutaRepository {

	@Override
	public Ruta adicionarNuevaRuta(Ruta ruta) throws RepositoryException {
		List<Tramo> recorrido = ruta.getRecorrido();
		
		TramoDAO tramoDAO = DAOFactory.getInstance().getTramoDAO(TipoDB.HSQLDB);
		RecorridoDAO recorridoDAO = DAOFactory.getInstance().getRecorridoDAO(TipoDB.HSQLDB);
		
		if(recorrido != null){
			for (Tramo tramo : recorrido) {
				tramoDAO.create(tramo);
				recorridoDAO.create(ruta.getId(), tramo.getId());
			}
		}
		return ruta;
	}

	@Override
	public Ruta obtenerRuta(Ruta parametro) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void actualizarRuta(Ruta ruta) {
		// TODO Auto-generated method stub
		
	}

}
