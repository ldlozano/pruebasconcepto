package co.metrobus.rasbus.infra.util;

public enum TipoDB {
	MEMORY, HSQLDB;
}
