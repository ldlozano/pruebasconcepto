var AppRouter = Backbone.Router.extend({
	
	routes: {
	    "rutas":   "rutas", 
	    "": "home"
	},
	
	initialize: function(){
		this.ruta = new Ruta();
		Backbone.history.start();
	},
	
	rutas: function(){
		var rutaView = new RutaView({model: this.ruta});
	},
	
	home: function(){
		alert("No puedes acceder");
	}
	
});