var Ruta = Backbone.Model.extend({

	initialize: function(){
		//Elementos para inicializar la ruta
	},

	defaults: {
		id: '1',
		nombre: 'Ruta 1'
	},
	
	url : function(){
		return "http://localhost:8081/rasbus/api/rutas/" + this.get("id");
	}
	
});

