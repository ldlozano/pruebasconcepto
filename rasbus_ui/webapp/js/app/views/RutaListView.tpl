Id Ruta: <%=ruta.id%> <br/>
Nombre Ruta: <%=ruta.nombre%> <br/>
Recorrido: <br/>
<%if(ruta.recorrido){
%>
<ul>
   <% _.each(ruta.recorrido.models, function(tramo){ %>
	<li>
		<%=tramo.get('inicio')%>, <%=tramo.get('destino')%> , <%=tramo.get('distancia')%> 
	</li>
   <% }) %>
</ul>
<%}%>