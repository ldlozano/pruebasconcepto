var RutaView = Backbone.View.extend({
	
	el: "body",

	events: {
		'click #ejecutar': "recuperar"
	},
	
	initialize: function(){
		this.listenTo(this.model, "change", this.render);
	},
	
	render: function() {
		var self = this;
		$.get("js/app/views/RutaListView.tpl", function(data){
			console.log("Renderizando Ruta " + JSON.stringify(self.model.attributes));
			template = _.template(data);
			$("#content", self.el).html(template({ruta: self.model.attributes}));
		})
	},
	
	
	recuperar: function(){
		this.model.set("id", 1);
		this.model.fetch();
	}
	
	

});