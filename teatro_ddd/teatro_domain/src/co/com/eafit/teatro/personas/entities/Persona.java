package co.com.eafit.teatro.personas.entities;

public abstract class Persona {
	private String id;
	private String nombre;
	private String email;
	private String telefono;
	public Persona(String id, String nombre, String email, String telefono) {
		super();
		this.id = id;
		this.nombre = nombre;
		this.email = email;
		this.telefono = telefono;
	}

}
