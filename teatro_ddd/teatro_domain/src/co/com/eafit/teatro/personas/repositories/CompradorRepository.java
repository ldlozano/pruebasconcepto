package co.com.eafit.teatro.personas.repositories;

import co.com.eafit.teatro.personas.entities.Comprador;

public interface CompradorRepository {
	
	public Comprador getComprador(String id);
}
