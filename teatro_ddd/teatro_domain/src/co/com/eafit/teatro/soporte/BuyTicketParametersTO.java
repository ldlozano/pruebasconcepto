package co.com.eafit.teatro.soporte;

import java.util.List;

public class BuyTicketParametersTO {
	private String comprador;
	private List<String> ubicacion;
	public String getComprador() {
		return comprador;
	}
	public void setComprador(String comprador) {
		this.comprador = comprador;
	}
	public List<String> getUbicacion() {
		return ubicacion;
	}
	public void setUbicacion(List<String> ubicacion) {
		this.ubicacion = ubicacion;
	}
	
}
