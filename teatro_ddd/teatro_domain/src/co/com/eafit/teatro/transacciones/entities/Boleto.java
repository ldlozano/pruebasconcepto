package co.com.eafit.teatro.transacciones.entities;

import co.com.eafit.teatro.transacciones.vo.Precio;
import co.com.eafit.teatro.transacciones.vo.Ubicacion;

public class Boleto {
	private String id;
	private Precio precio;
	private Funcion funcion;
	private Localidad localidad;
	private Ubicacion ubicacion;
	private int estado;
	
	
	public void modificarEstado(int nuevoEstado){
		this.estado = nuevoEstado;
	}
	
}
