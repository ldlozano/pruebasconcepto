package co.com.eafit.teatro.transacciones.vo;

public class Precio {
	private float valor;

	public Precio(float valor) {
		super();
		this.valor = valor;
	}

	public float getValor() {
		return valor;
	}

}
